create database bestbuy_dropshiping;

create table bestbuy(
    sku int primary key,
    model text not null,
    title text not null,
    category text not null,
    inventory boolean not null,
    price numeric not null,
    original_price numeric not null,
    discount boolean not null,
    link text not null,
    thumb text not null,
    description text not null
);

create table bestbuy_includes(
    sku int references bestbuy(sku),
    body text not null
)

create table bestbuy_features(
    sku int references bestbuy(sku),
    title text not null,
    body text not null
)

create table bestbuy_specs(
    sku int references bestbuy(sku),
    title text not null,
    name text not null,
    value text not null
)

create table bestbuy_images(
    sku int references bestbuy(sku),
    link text not null
);

create table posts(
    id text primary key,
    title text not null,
    category text not null,
    inventory int not null,
    price numeric not null,
    is_premium boolean not null,
    status text not null,
    description text not null,
    account_id int not null,
    vendor_id text not null,
    item_id text not null
);

-- DOWN
-- drop table bestbuy_includes;
-- drop table bestbuy_features;
-- drop table bestbuy_specs;
-- drop table bestbuy_img;
-- drop table bestbuy;
-- drop table posts; 