Un projecto viejo, lo dejo aqui para referencias futuras ya que debido a cambios en la API de Mercado Libre y la pagina de Best Buy Mexico probablemente no funciona correctamente.

La idea es usar un Scrapper para sacar info de todos los productos en Best Buy Mexico y crear nuevas publicaciones en MercadoLibre para cada uno de los productos.

Despues de publicar los productos por primera ves cree un script para sincronisar informacion nueva como precios, stock y nuevos productos.

Todo el codigo esta creado para ser efimero. La base de datos puede borrarse completamente y volverse a rellenar con informacion de MercadoLibre y Best Buy.