const axios = require("axios");
const load = require("cheerio").load;
const sample = require("lodash.samplesize");
const exec = require("child_process").exec;
const range = require("lodash.range");
const Postgres = require("pg").Client;
const serial = require("promise-serial");

const dev = process.env.NODE_ENV !== "production";

// for some reason request and axios wont return other pages, only curl
function curlGet(link) {
    return new Promise((resolve, reject) => {
        const conf = {maxBuffer: 1024 * 500};
        exec("curl " + `"${link}"`, conf, (err, stdout) => {
            if (err) reject(err);
            else resolve(stdout);
        });
    });
}

async function getCategories() {
    const html = await axios.get("https://www.bestbuy.com.mx/").then(x => x.data);

    const $ = load(html);

    let categories_acc = [];

    const selector = ".shopby li.js-navitem .sub-cat-links";
    $(selector).each((i, el) => {
        let categories = [];
        $(el)
            .find(".column-wrap a")
            .each((i, el) => {
                const href = $(el).attr("href");

                if (/\?.*$/.test(href) && !href.includes("brand_facet")) return;

                const name = href
                    .replace(/\?.*$/, "")
                    .replace(/\/c[0-9]+/, "")
                    .split("/")
                    .slice(-1)[0]
                    .replace(/-/g, " ");

                categories.push({link: href, name});
            });
        // the last one is the "all" category
        categories_acc = categories_acc.concat(categories.slice(0, -1));
    });

    return categories_acc;
}

let saved = [];

(async function () {
    const pg = new Postgres({database: "bestbuy_dropship"});
    await pg.connect();

    if (!dev) {
        await pg.query("delete from bestbuy_includes");
        await pg.query("delete from bestbuy_features");
        await pg.query("delete from bestbuy_specs");
        await pg.query("delete from bestbuy_img");
        await pg.query("delete from bestbuy");
    }

    let categories;
    categories = await getCategories();
    // if (dev) categories = sample(categories)
    if (dev) categories = sample(categories.slice(0, 1));

    for (let i = 0; i < categories.length; i++) {
        const cat = categories[i];
        const html = await axios.get(cat.link).then(x => x.data);

        let page = 1;
        while (true) {
            const link = [
                cat.link,
                "?query=categoryId%24c91",
                "&sort=Best-Match",
                `&page=${page}`
            ].join("");

            console.log(`processing ${cat.name}, page ${page}`);
            console.log(link);

            const html = await curlGet(link);

            const $ = load(html);

            if (html.includes("Búsqueda sin resultados.")) break;

            let items = [];
            $(".product-line-item-line").each((i, el) => {
                const sku = $(el)
                    .find(".product-skuId .sku-value")
                    .text()
                    .trim();

                // ignore if duplicated
                if (saved.includes(sku)) return;
                else saved.push(sku);

                const link = $(el)
                    .find(".image-container a")
                    .attr("href");
                const thumbnail = $(el)
                    .find(".image-container img")
                    .attr("src");
                const title = $(el)
                    .find(".product-title h4")
                    .text();
                const model = $(el)
                    .find(".product-modelNumber .sku-value")
                    .text()
                    .trim();
                const inventory =
                    $(el)
                        .find(".cart-button-wrapper button")
                        .attr("disabled") !== "disabled";
                const originalPrice = $(el)
                    .find(".product-regprice")
                    .text()
                    .replace(/\$|,|Antes/g, "")
                    .trim();
                const price = $(el)
                    .find(".product-price")
                    .text()
                    .replace(/\$|,/g, "");
                const discount = originalPrice !== "";

                items.push({
                    link,
                    thumbnail,
                    title,
                    category: cat.name,
                    sku,
                    model,
                    inventory,
                    price: Number(price),
                    originalPrice: discount ? Number(originalPrice) : Number(price),
                    discount
                });
            });

            console.log(
                `processing done for "${cat.name}" page ${page} found ${
                    items.length
                    } items`
            );

            // if (dev) items = sample(items)
            if (dev) items = sample(items.slice(0, 1));

            await serial(
                items.map(item => async () => {
                    let html;

                    try {
                        html = await axios.get(item.link).then(x => x.data);
                    } catch (error) {
                        console.log("error:", item.sku);
                        console.log(error);
                        return;
                    }

                    const $ = load(html);

                    let images = [];
                    $(".carousel-trigger img").each((i, el) => {
                        images.push($(el).attr("src"));
                    });

                    const description = $(".bbmx-product-description p").text();

                    let includes = [];
                    $(".bbmx-included-items li").each((i, el) => {
                        includes.push($(el).text());
                    });

                    let features = [];
                    $(".features-container .list-row").each((i, el) => {
                        const title = $(el)
                            .find("h3")
                            .text();
                        const body = $(el)
                            .find("p")
                            .text();
                        features.push({title, body});
                    });

                    let specs = [];
                    // for multiple specs
                    $(".specifications-container").each((i, el) => {
                        const title = $(el)
                            .find(".specifications-title")
                            .text();

                        $(el)
                            .find("tr.specification-row")
                            .each((i, el) => {
                                const name = $(el)
                                    .find("td.specification-name")
                                    .text();
                                const value = $(el)
                                    .find("td.specification-value")
                                    .text();
                                specs.push({title, name, value});
                            });
                    });
                    // for products with only a list of specs
                    $(".specifications-list-container").each((i, el) => {
                        const title = $(el)
                            .find("h4")
                            .text();

                        $(el)
                            .find(".specification-list-row")
                            .each((i, el) => {
                                const name = $(el)
                                    .find(".specification-list-name")
                                    .text();
                                const value = $(el)
                                    .find(".specification-list-value")
                                    .text();
                                specs.push({title, name, value});
                            });
                    });

                    const values = [
                        item.sku,
                        item.model,
                        item.title,
                        item.category,
                        item.inventory,
                        item.price,
                        item.originalPrice,
                        item.discount,
                        item.link,
                        item.thumbnail,
                        description
                    ];

                    const query = `
                    insert into bestbuy values 
                    (${range(1, values.length + 1).map(x => "$" + x)})
                `;

                    if (!dev) {
                        await pg.query(query, values);
                    } else {
                        console.log(query, values);
                    }

                    for (let i = 0; i < includes.length; i++) {
                        const include = includes[i];

                        const values = [item.sku, include];

                        const query = `
                        insert into bestbuy_includes
                        values (${range(1, values.length + 1).map(
                            x => "$" + x
                        )})
                    `;

                        if (!dev) {
                            await pg.query(query, values);
                        } else {
                            console.log(query, values);
                        }
                    }

                    for (let i = 0; i < features.length; i++) {
                        const feature = features[i];

                        const values = [item.sku, feature.title, feature.body];

                        const query = `
                        insert into bestbuy_features
                        values (${range(1, values.length + 1).map(
                            x => "$" + x
                        )})
                    `;

                        if (!dev) {
                            await pg.query(query, values);
                        } else {
                            console.log(query, values);
                        }
                    }

                    for (let i = 0; i < specs.length; i++) {
                        const spec = specs[i];

                        const query = `
                        insert into bestbuy_specs
                        values (${range(1, 5).map(x => "$" + x)})
                    `;
                        const values = [item.sku, spec.title, spec.name, spec.value];

                        if (!dev) {
                            await pg.query(query, values);
                        } else {
                            console.log(query, values);
                        }
                    }

                    for (let i = 0; i < images.length; i++) {
                        const img = images[i];

                        const query = "insert into bestbuy_img values ($1, $2)";
                        const values = [item.sku, img];

                        if (!dev) {
                            await pg.query(query, values);
                        } else {
                            console.log(query, values);
                        }
                    }

                    if (dev) {
                        console.log({
                            ...item,
                            description,
                            includes,
                            features,
                            specs,
                            images
                        });
                    }

                    console.log(`[${item.sku}] ${item.title}`);
                }),
                {parallelize: 20}
            );

            if (dev) break;
            if ($(".pagination-controls li").length === 0) break;

            page++;
        }
    }
})();
