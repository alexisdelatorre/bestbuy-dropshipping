const Postgres = require("pg").Client;
const sample = require("lodash.samplesize");
const Logger = require("../utils/logger").Client;
const Meli = require("../utils/meli").Client;
const parseCsv = require("d3-dsv").csvParse;
const readFile = require("fs").readFileSync;
const serial = require("promise-serial");

const calcPrice = require("../utils/price");
const genDesc = require("../utils/description");

const dev = process.env.NODE_ENV !== "production";

function cleanDesc(desc) {
    return desc
        .split("\n")
        .map(x => x.trim())
        .join("\n")
        .trim();
}

let shippingCache = {};

(async function () {
    const pg = new Postgres({database: "bestbuy_dropship"});
    await pg.connect();

    const meli = new Meli();
    await meli.authenticate();

    const categories = parseCsv(readFile("./data/categories.csv", "utf-8"));

    // console.log('Getting all the items, this may take a while')

    // const itemsQ = `
    //     select
    //         bb.*,
    //         jsonb_agg(distinct images) as images,
    //         jsonb_agg(distinct features) as features,
    //         jsonb_agg(distinct includes) as includes,
    //         jsonb_agg(distinct specs) as specs
    //     from bestbuy as bb
    //     left join bestbuy_img as images on images.sku = bb.sku
    //     left join bestbuy_features as features on features.sku = bb.sku
    //     left join bestbuy_includes as includes on includes.sku = bb.sku
    //     left join bestbuy_specs as specs on specs.sku = bb.sku
    //     group by bb.sku
    // `
    // const items = await pg.query(itemsQ)
    //     .then(x => x.rows)

    let posts;
    // posts = await pg.query('select * from posts where id = $1', ['MLM646205058'])
    posts = await pg.query("select * from posts").then(x => x.rows);
    if (dev) posts = sample(posts);

    const logger = new Logger(posts.length);

    await serial(
        posts.map(post => async () => {
            // console.log(post.id)

            const log = logger.log([post.id]);

            const pause = async () => {
                if (post.inventory < 1) return;

                const query = "update posts set inventory = $1 where id = $2";
                const values = [0, post.id];

                const body = {inventory: 0};

                if (!dev) {
                    try {
                        await meli.update(post.id, body);
                        await pg.query(query, values);
                    } catch (error) {
                        log("error updating inventory");
                        console.log(error);

                    }
                } else {
                    console.log(query, values);
                    console.log("update meli", post.id, body);
                }
            };

            log("paused");
            await pause();
            return;

            if (post.status === "under_review") {
                log("item under review");
                return;
            }

            if (post.status === "closed") {
                log("item closed");
                return;
            }

            const item = await pg
                .query("select * from bestbuy where sku = $1", [post.item_id])
                .then(x => x.rows[0]);
            let images = await pg
                .query("select * from bestbuy_img where sku = $1", [post.item_id])
                .then(x => x.rows);
            let features = await pg
                .query("select * from bestbuy_features where sku = $1", [post.item_id])
                .then(x => x.rows);
            let specs = await pg
                .query("select * from bestbuy_specs where sku = $1", [post.item_id])
                .then(x => x.rows);
            let includes = await pg
                .query("select * from bestbuy_includes where sku = $1", [post.item_id])
                .then(x => x.rows);

            if (!item) {
                log("no item");
                await pause();
                return;
            }

            if (specs.filter(x => x.value.includes("Bose")).length > 0) {
                log("bose");
                await pause();
                return;
            }

            const category = categories.filter(
                x => x.category_bb === item.category
            )[0];

            let shipping;

            if (category.status !== "publicar" || category.shipping !== "me") {
                shipping = 1000;
            } else if (!shippingCache[category.category_meli]) {
                shipping = await meli.getShippingPrice(category.category_meli);
                shippingCache[category.category_meli] = shipping;
            } else if (shippingCache[category.category_meli]) {
                shipping = shippingCache[category.category_meli];
            }

            const price = calcPrice({
                vendorPrice: Number(item.price),
                shipping,
                isPremium: false
            });

            let msg = [];

            if (Number(post.price) !== price) {
                const query = "update posts set price = $1 where id = $2";
                const values = [price, post.id];

                const body = {price};

                if (!dev) {
                    try {
                        await meli.update(post.id, {price});
                        await pg.query(query, values);
                    } catch (error) {
                        log("error updating price");
                        console.log(error);
                        return;
                    }
                } else {
                    console.log(query, values);
                    console.log("update meli", post.id, body);
                }

                msg.push(`price: ${post.price} -> ${price}`);
            }

            let inventory;
            inventory = item.inventory ? 5 : 0;
            if (category.status !== "publicar" || category.shipping !== "me") {
                inventory = 0;
            }

            if (post.inventory !== inventory) {
                const query = "update posts set inventory = $1 where id = $2";
                const values = [inventory, post.id];

                const body = {inventory};

                if (!dev) {
                    try {
                        await meli.update(post.id, body);
                        await pg.query(query, values);
                    } catch (error) {
                        log("error updating inventory");
                        console.log(error);
                        return;
                    }
                } else {
                    console.log(query, values);
                    console.log("update meli", post.id, body);
                }

                msg.push(`inventory: ${post.inventory} -> ${inventory}`);
            }

            const meliCat =
                category.status !== "publicar" ? "MLM1009" : category.category_meli;

            if (post.category !== meliCat) {
                const query = "update posts set category = $1 where id = $2";
                const values = [meliCat, post.id];

                const body = {category: meliCat};

                if (!dev) {
                    try {
                        await meli.update(post.id, body);
                        await pg.query(query, values);
                    } catch (error) {
                        log("error updating inventory");
                        console.log(error.response.data);
                        return;
                    }
                } else {
                    console.log(query, values);
                    console.log("update meli", post.id, body);
                }

                msg.push(`category: ${post.category} -> ${meliCat}`);
            }

            if (!features[0]) {
                features = [{sku: "", body: "", title: ""}];
            }

            if (!includes[0]) {
                includes = [{sku: "", body: ""}];
            }

            const carrier = specs.filter(x => x.name === "Compañía telefónica")[0];

            const description = genDesc({
                originalTitle: item.title,
                description: item.description,
                includes,
                features,
                specs,
                carrier,
                sku: item.sku
            });

            if (cleanDesc(post.description) !== description) {
                const query = "update posts set description = $1 where id = $2";
                const values = [description, post.id];

                if (!dev) {
                    try {
                        await meli.updateDesc(post.id, description);
                        await pg.query(query, values);
                    } catch (error) {
                        log("error updating inventory");
                        console.log(error.response.data);
                        return;
                    }
                } else {
                    console.log("update desc");
                }

                msg.push(`description`);
            }

            if (msg.length > 0) log(msg.join("; "));
            else log("no changes");
        }),
        {parallelize: 15}
    );
})().catch(console.log);
