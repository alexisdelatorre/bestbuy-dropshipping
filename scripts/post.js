const Postgres = require('pg').Client;
const Meli = require('../utils/meli').Client;
const Logger = require('../utils/logger').Client;
const sample = require('lodash.samplesize');
const auth = require('meli-auth');
const range = require('lodash.range');
const serial = require('promise-serial');
const parseCsv = require('d3-dsv').csvParse;
const readFile = require('fs').readFileSync;

const truncateTitle = require('../utils/title');
const calcPrice = require('../utils/price');
const genDesc = require('../utils/description');

const dev = process.env.NODE_ENV !== 'production';

let shippingCache = {}

;
(async function () {
    const pg = new Postgres({database: 'bestbuy_dropship'});
    await pg.connect();

    const token = await auth({}).promise();
    const meli = new Meli({token});
    await meli.authenticate();

    const categories = parseCsv(readFile('./data/categories.csv', 'utf-8'));

    const query = `
        select 
            bb.*,
            jsonb_agg(distinct images) as images,
            jsonb_agg(distinct features) as features,
            jsonb_agg(distinct includes) as includes,
            jsonb_agg(distinct specs) as specs
        from bestbuy as bb
        left join bestbuy_img as images on images.sku = bb.sku
        left join bestbuy_features as features on features.sku = bb.sku
        left join bestbuy_includes as includes on includes.sku = bb.sku
        left join bestbuy_specs as specs on specs.sku = bb.sku
        group by bb.sku
    `;

    let items;
    items = await pg.query(query)
        .then(x => x.rows);
    if (dev) items = sample(items);

    const logger = new Logger(items.length);

    await serial(items.map(item => async () => {
        // console.log(item.sku)

        const log = logger.log([item.sku]);

        const category = categories.filter(x => x.category_bb === item.category)[0];

        const posts = await pg.query(
            'select * from posts where item_id = $1', 
            [item.sku]
        )
            .then(x => x.rows);

        if (posts.length > 0) {
            log('sku already posted');
            return
        }

        const images = await pg.query(
            'select * from bestbuy_img where sku = $1',
            [item.sku]
        )
            .then(x => x.rows.map(x => x.link).slice(0, 12));

        let features = item.features;
        if (features[0] === null) {
            features = [{ sku: '', body: '', title: '' }] 
        }

        let includes = item.includes;
        if (includes[0] === null) {
            includes = [{ sku: '', body: '' }] 
        }

        const title = truncateTitle(item.title);
        let inventory = item.inventory ? 5 : 0;
        const description = genDesc({
            originalTitle: item.title,
            description: item.description,
            includes,
            features,
            specs: item.specs,
            sku: item.sku,
        });
        let meliCat;

        let shipping;

        if (!category || category.status !== 'publicar' || category.shipping !== 'me') {
            inventory = 0;
            shipping = 1000;
            meliCat = 'MLM1009'
        }
        else if (!shippingCache[category.category_meli]) {
            shipping = await meli.getShippingPrice(category.category_meli);
            shippingCache[category.category_meli] = shipping;
            meliCat = category.category_meli
        }
        else if (shippingCache[category.category_meli]) {
            shipping = shippingCache[category.category_meli];
            meliCat = category.category_meli
        }

        const price = calcPrice({
            vendorPrice: Number(item.price),
            shipping,
            isPremium: false,
        });

        const body = {
            title,
            description,
            inventory: inventory > 0 ? inventory : 1,
            price,
            category: meliCat,
            images,
            isPremium: false,
            usesMe: true,
        };

        const query = `insert into posts values (${range(1, 12).map(x => `$${x}`)})`;

        const userId = await meli.getUser().then(x => x.id);

        const values = [
            title,
            meliCat,
            inventory,
            price,
            false,
            'active',
            description,
            userId,
            'BB',
            item.sku,
        ];

        let postId;

        if (!dev) {
            try {
                postId = await meli.post(body);
                if (inventory < 1) await meli.update(postId, {inventory});
                await pg.query(query, [postId].concat(values))
            } catch (error) {
                log('error');
                console.log(error.response.data);
                return
            }
        }
        else {
            postId = 'MLM######'
            // console.log(query, ['sku'].concat(values))
            // console.log(body)
        }

        // if (dev) console.log(item)

        log(`posted: ${postId} ; ${title}`)
    }), { parallelize: 15 })
})().catch(console.log);