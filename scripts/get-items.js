const Postgres = require("pg").Client;
const Meli = require("../utils/meli");
const Logger = require("../utils/logger").Client;
const sample = require("lodash.samplesize");
const range = require("lodash.range");
const serial = require("promise-serial");

const dev = process.env.NODE_ENV !== "production";

(async function () {
    const pg = new Postgres({database: "bestbuy_dropship"});
    await pg.connect();

    if (!dev) pg.query("delete from posts");

    const meli = new Meli.Client();
    await meli.authenticate();

    const total = await meli.getSellerItems().then(x => x.total);

    const logger = new Logger(total);

    let count = 0;
    let limit;
    if (Math.floor(total / 50) > 14) limit = sample(range(1, 15))[0];
    else limit = Math.floor(total / 50);

    let scroll;

    while (true) {
        let items;

        await meli.getSellerItems(scroll).then(x => {
            items = x.items;
            scroll = x.scroll;
        });

        if (dev) items = sample(items);

        if (items.length === 0) break;

        if (dev && count < limit) {
            count++;
            continue;
        }

        await serial(
            items.map(itemId => async () => {
                const log = logger.log([itemId]);

                let item;
                try {
                    item = await meli.getItem(itemId);
                } catch (error) {
                    log("error");
                    console.log(error);
                    return;
                }

                const lastLine = item.description.split("\n").slice(-1)[0];

                if (!lastLine.includes(";;")) {
                    log('not including ";;" on last line');
                    return;
                }

                const user_id = await meli.getUser().then(x => x.id);

                const values = [
                    item.id,
                    item.title,
                    item.category,
                    item.inventory,
                    item.price,
                    item.premium,
                    item.status,
                    item.description,
                    user_id,
                    lastLine.split(";;")[0],
                    lastLine.split(";;")[1]
                ];

                const query = `insert into posts values (${range(1, 12).map(
                    x => "$" + x
                )})`;

                if (!dev) {
                    try {
                        await pg.query(query, values);
                    } catch (error) {
                        log("error");
                        // console.log(error)
                        return;
                    }
                } else {
                    console.log(query, values);
                }

                log(item.title);
            }),
            {parallelize: 15}
        );

        count++;
        if (count > Math.floor(total / 50)) break;
        if (dev) break;
    }
})();
