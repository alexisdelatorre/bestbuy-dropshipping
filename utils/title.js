function truncateTitle (title) {
    if (title.length < 61) return title.toLowerCase()

    let truncated

    truncated = title
        .toLowerCase()
        .replace(/-|–/g, ' ')
        .replace(/ a | de | y | para | en |\/| con |"|\+/g, ' ')
        .replace(/\/|\.|\:|\(|\)/g, '')
        .replace(/ +/g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated
        .replace(/disco duro/g, 'dd')
        .replace(/memoria/g, 'mem')
        .replace(/geforce|nvidia|intel/g, '')
        .replace(/core i3/g, 'i3')
        .replace(/core i7/g, 'i7')
        // .replace(/azul/g, 'az')
        // .replace(/blanco/g, 'bl')
        .replace(/negro/g, 'ngo')
        .replace(/plata/g, 'pl')
        // .replace(/gris oscuro/g, 'gr')
        .replace(/gris espacial/g, 'gr')
        .replace(/dorado/g, 'dor')
        // .replace(/naranja/g, 'nrj')
        // .replace(/amarillo/g, 'am')
        // .replace(/paquete/g, 'pqt')
        // .replace(/juego/g, 'jgo')
        .replace(/pokemon/g, 'pkm')
        // .replace('distintos colores', '')
        .replace('unidad estado sólido', 'ssd')
        .replace(/ +/g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated.replace(/ \w{14,} /g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated.replace(/ \w{12,} /g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated.replace(/ \w{10,} /g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated.replace(/ \w{8,} /g, ' ')

    if (truncated.length < 61) return truncated

    truncated = truncated.split(' ').slice(1).join(' ')

    if (truncated.length < 61) return truncated
    
    truncated = truncated.replace(/ \w{6} /g, '')

    if (truncated.length < 61) return truncated

    truncated = truncated.replace(/mem [0-9]gb|mem [0-9] gb/g, '')

    return truncated
}

module.exports = truncateTitle