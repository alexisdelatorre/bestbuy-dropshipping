function Client (total) {
    this.total = total
    this.count = 0
} 

Client.prototype.log = function (prefixes) {
    return (msg, ignoreCount) => {
        if (!ignoreCount) this.count++

        const progress = `[${this.count}/${this.total}]`
        const prefix = [progress]
            .concat(prefixes.map(x => `[${x}]`))
            .join(' ')
        console.log(prefix, msg)
    }
}

module.exports = { Client }