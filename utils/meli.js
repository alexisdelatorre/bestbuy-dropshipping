const auth = require('meli-auth')
const get = require('axios').get
const put = require('axios').put
const post = require('axios').post
const qs = require("querystring").stringify

const shippingMe = {
    mode: 'me2',
    local_pick_up: false,
    free_shipping: true,
    free_methods: [{ id: 501245, rule: { free_mode: 'country', value: null } }],
    dimensions: null,
    tags: ['me2_available'],
    logistic_type: 'drop_off',
    store_pick_up: false
  }
  
const shipping = {
    "mode": "not_specified",
    "dimensions": null,
    "local_pick_up": false,
    "free_shipping": true,
    "logistic_type": "not_specified",
    "store_pick_up": false
}

function Client (args) {
    // if (args.token) this.token = args.token
    // else throw new Error('should initialize with a meli token')

    this.host = 'https://api.mercadolibre.com'
}

Client.prototype.authenticate = async function () {
    this.token = await auth({}).promise()
    this.user = await this.getUser()
}

Client.prototype.post = async function (args) {
    const link = `${this.host}/items?access_token=${this.token}`

    const body = {
        title: args.title,
        description: { plain_text: args.description },
        price: args.price,
        available_quantity: args.inventory,
        category_id: args.category,
        pictures: args.images.map(source => ({ source })),
        listing_type_id: args.isPremium ? 'gold_pro': 'gold_special',
        shipping: args.usesMe ? shippingMe : shipping,
        currency_id: 'MXN',
        condition: 'new',
    }

    return post(link, body).then(x => x.data.id)
}

Client.prototype.update = async function (id, args) {
    const link = `${this.host}/items/${id}?access_token=${this.token}`

    const body = {}

    if (args.price !== undefined) {
        body.price = args.price
    }

    if (args.inventory !== undefined) {
        body.available_quantity = args.inventory
    }

    if (args.category !== undefined) {
        body.category_id = args.category
    }

    await put(link, body)
}

Client.prototype.updateDesc = async function (id, newDesc) {
    const link = `${this.host}/items/${id}/description?access_token=${this.token}`
    const body = { plain_text: newDesc }
    await put(link, body)
}

Client.prototype.getUser = async function () {
    if (!this.user) {
        const link = `${this.host}/users/me?access_token=${this.token}`
        return get(link).then(x => x.data)
    }
    else {
        return this.user
    }
}

Client.prototype.getItem = async function (postId) {
    const itemLink = `${this.host}/items/${postId}`
    const descLink = `${this.host}/items/${postId}/description`

    const post = await get(itemLink)
        .then(x => x.data)

    const id = post.id
    const title = post.title
    const category = post.category_id
    const inventory = post.initial_quantity || 0
    const price = post.price || 0
    const premium = post.listing_type_id === 'gold_pro'
    const status = post.status

    const description = await get(descLink)
        .then(x => x.data.plain_text)

    return {
        id,
        title,
        category,
        inventory,
        price,
        premium,
        status,
        description
    }
}

Client.prototype.getSellerItems = async function (_scroll) {
    let link
    link = [
        this.host,
        '/users',
        `/${this.user.id}`,
        '/items',
        '/search',
        '?search_type=scan',
        `&access_token=${this.token}`,
    ].join('')
    if (_scroll) link += `&scroll_id=${_scroll}`

    const search = await get(link)
        .then(x => x.data)

    const items = search.results
    const scroll = search.scroll_id
    const total = search.paging.total

    return { items, scroll, total }
}

Client.prototype.getShippingPrice = async function (category) {
    const dimLink = `${this.host}/categories/${category}/shipping`

    let dimensions
    dimensions = await get(dimLink)
    dimensions = dimensions.data

    const { height, width, length, weight } = dimensions

    const link = [
        this.host,
        '/sites',
        '/MLM',
        '/shipping_options',
        '/free',
        '?dimensions=',
        `${height}x${width}x${length},${weight}`,
    ].join('')

    let shipping_price
    shipping_price = await get(link)
    shipping_price = shipping_price.data
    shipping_price = shipping_price.coverage.all_country.list_cost

    return shipping_price
}

Client.prototype.updateStatus = async function (id, status) {
    const link = `${this.host}/items/${id}?access_token=${this.token}`
    await put(link, { status })
}

Client.prototype.delete = async function (id) {
    const link = `${this.host}/items/${id}?access_token=${this.token}`
    await put(link, { deleted: true })
}

Client.prototype.predictCat = async function (title) {
    const link = `${this.host}/sites/MLM/category_predictor/predict?title=${qs({title})}`
    return get(link).then(x => x.data.id)
}

module.exports = { Client }

// const get = require('axios').get
// const post = require('axios').post

// const shippingMe = {
//     mode: 'me2',
//     local_pick_up: false,
//     free_shipping: true,
//     free_methods: [
//       {
//         id: 501245,
//         rule: {
//           free_mode: 'country',
//           value: null
//         }
//       }
//     ],
//     dimensions: null,
//     tags: ['me2_available'],
//     logistic_type: 'drop_off',
//     store_pick_up: false
//   }
  
// const shipping = {
//     "mode": "not_specified",
//     "dimensions": null,
//     "local_pick_up": false,
//     "free_shipping": true,
//     "logistic_type": "not_specified",
//     "store_pick_up": false
// }

// function Client (args) {
//     this.token = args.token

//     this.host = 'https://api.mercadolibre.com'
// }

// Client.prototype.post = async function (args) {
//     const link = `${this.host}/items?access_token=${this.token}`

//     const body = {
//         title: args.title,
//         description: { plain_text: args.description },
//         price: args.price,
//         available_quantity: args.inventory,
//         category_id: args.category,
//         pictures: args.images.map(source => ({ source })),
//         listing_type_id: args.isPremium ? 'gold_pro': 'gold_special',
//         shipping: args.usesMe ? shippingMe : shipping,
//         currency_id: 'MXN',
//         condition: 'new',
//     }

//     return post(link, body)
//         .then(x => x.data.id)
// }

// Client.prototype.update = async function (args) {
//     const body = {}

//     if (args.price) {
//         body.price = args.price
//     }

//     console.log('update:', body)
// }

// Client.prototype.predictCat = async function (title) {
//     const link = `${this.host}/sites/MLM/category_predictor/predict?title=${qs({title})}`
//     return get(link).then(x => x.data.id)
// }

// module.exports = { Client }