function calcPrice (args) {
    const vendorPrice = args.vendorPrice
    const shipping = args.shipping
    const isPremium = args.isPremium

    const expectedProfit = 0.10

    const commMeli = isPremium ? 0.175 : 0.13
    const lessThan550 = 0.30
    const moreThan550 = 0.50

    // console.log()

    let pv

    const a = (vendorPrice + (shipping * (1 - lessThan550))) / (1 - commMeli - 0.10)
    if (a < 550) pv = a

    const b = (vendorPrice + (shipping * (1 - moreThan550))) / (1 - commMeli - 0.10)
    if (b > 549 && b < 1000) pv = b

    if (a > 549 && b < 1000) pv = a

    const c = (vendorPrice + (shipping * (1 - moreThan550))) / (1 - commMeli - 0.08)
    if (c > 999 && c < 4000) pv = c

    if (b > 999 && c < 4000) pv = c

    const d = (vendorPrice + (shipping * (1 - moreThan550))) / (1 - commMeli - 0.06)
    if (d > 3999 && d < 10000) pv = d

    if (c > 3999 && d < 10000) pv = c

    const e = (vendorPrice + (shipping * (1 - moreThan550))) / (1 - commMeli - 0.05)
    if (e > 9999) pv = e

    if (d > 10000 && e < 10000) pv = e

    // console.log({a,b,c,d,e})
    // console.log({pv})

    // // console.log(pv - (pv * commMeli) - (shipping * (1 - lessThan550)))
    // // console.log((pv - (pv * commMeli) - (shipping * (1 - lessThan550))) / pv)

    // // console.log()

    // console.log(pv - vendorPrice - (pv * commMeli) - (shipping * (1 - moreThan550)))
    // console.log((pv - vendorPrice - (pv * commMeli) - (shipping * (1 - moreThan550))) / pv)

    return Number(pv.toFixed(2))
}

module.exports = calcPrice