function genDesc (args) {
    const originalTitle = args.originalTitle;
    const description = args.description;
    const includes = args.includes;
    const features = args.features;
    const specs = args.specs;
    const carrier = args.carrier;
    const sku = args.sku;

    if (carrier) {
        return `
            ====================

            ====================
            ${carrier.value !== 'Desbloqueado' ?  `
                Este telefono solo funciona con la compania ${carrier.value}.
        
                ====================
            `.split('\n').map(x => x.trim()).join('\n'): `
                Este telefono viene desbloqueado para cualquier compania.
        
                ====================
            `.split('\n').map(x => x.trim()).join('\n')}
            ${originalTitle}

            ====================

            ${description}

            ====================
            
            INCLUYE:

            ${includes.map(i => `- ${i.body}`).join('\n')}

            ====================

            CARACTERISTICAS:
            
            ${features.map(f => `-\n${f.title !== '' ? `${f.title}:\n`: ''}${f.body}`).join('\n\n')}

            ====================

            ESPECIFICACIONES:

            ${specs.map(sp => `- ${sp.name}: ${sp.value}`).join('\n')}

            ====================
            BB;;${sku}
        `.split('\n').map(x => x.trim()).join('\n').trim()
    }
    else {
        return `
        ====================

        ====================

        ${originalTitle}

        ====================

        ${description}

        ====================
        
        INCLUYE:

        ${includes.map(i => `- ${i.body}`).join('\n')}

        ====================

        CARACTERISTICAS:
        
        ${features.map(f => `-\n${f.title !== '' ? `${f.title}:\n`: ''}${f.body}`).join('\n\n')}

        ====================

        ESPECIFICACIONES:

        ${specs.map(sp => `- ${sp.name}: ${sp.value}`).join('\n')}

        ====================
        BB;;${sku}
    `.split('\n').map(x => x.trim()).join('\n').trim()
    }
}

module.exports = genDesc;